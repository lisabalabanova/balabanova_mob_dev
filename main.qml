import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12

ApplicationWindow {
    id: applicationWindow
    visible: true
    signal signalMakeRequest();
       signal success (string lab);
       signal failed(string lab);
        signal cancel (string lab);
        signal restRequest();
        signal hashvd(string lab);
    width: 480
    height: 640
    title: qsTr("Мобильная разработка")
    Drawer{
        id: drawer
        width: 0.66 * parent.width
        height: parent.height
        GridLayout{
        rows: 15

        Button{
        Layout.row: 1
        text: qsTr("Лаба 1")
        onClicked: {
            swipeView.currentIndex=0
            drawer.close()
        }
        }

        Button{
        Layout.row: 2
        text: qsTr("Лаба 2")
        onClicked: {
            swipeView.currentIndex=1
            drawer.close()
            }

        }

        Button{
            Layout.row: 3
            text: qsTr("Лаба 3")
            onClicked: {
                swipeView.currentIndex=2
                drawer.close()
            }
        }

        Button{
            Layout.row: 4
            text: qsTr("Лаба 4")
            onClicked: {
                swipeView.currentIndex=3
                drawer.close()
            }
        }

        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Page1 {
            id: page1
        }

        Page2 {
            id: page2
        }

        Page3 {
            id: page3
        }

        Page4 {
            id: page4
        }

        Page5 {
            id: page5
        }

        Page6 {
            id: page6
        }

        Page7 {
            id: page7
        }

        Page8 {
            id: page8
        }

        Page9 {
            id: page9
        }
    }



    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex




    }
}


