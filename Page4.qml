import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12

Page{
    id: page4
    header: ToolBar{
        contentHeight: 15
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 30
            implicitWidth: 100
            //главная картинка
            Image {
                id: image2222
                height: 102
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                fillMode: Image.Stretch
                source: "jell.png"
                //иконка галочек
                Image {
                    id: image1222
                    width: 50
                    height: 36
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 217
                    anchors.left: parent.left
                    anchors.leftMargin: 217
                    fillMode: Image.PreserveAspectFit
                    source: "tap.png"
                }
                //название лабы
                Label {
                    id: label222
                    height: 56
                    color: "#deffffff"
                    text: qsTr("Лабораторная работа №4.
    ")
                    font.pointSize: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    font.weight: Font.DemiBold
                    font.family: "Tahoma"
                    font.bold: true
                    styleColor: "#ffffff"
                    textFormat: Text.AutoText
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
    Rectangle {
        id: rectangle122
        color: "#1d2027"
        anchors.topMargin: 0
        anchors.fill: parent
    }
    GridLayout {
        anchors.topMargin: 82
        anchors.fill: parent
        columns: 2
        
        Button {
            id: sent
            Layout.alignment: Qt.AlignCenter
            Layout.columnSpan: 2
            onClicked: {
                _send.getNetworkValue();
            }
            
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Отправить")
            }
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 20
                opacity: enabled ? 1 : 0.3
                color: "#ffab25"
                radius: 10
            }
        }
        
        
        ScrollView {
            Layout.fillHeight: true
            Layout.columnSpan: 2
            Layout.fillWidth: true
            
            clip:  true
            TextArea{
                id: textArea
                // textFormat: Text.RichText
                objectName: "textArea"
                readOnly: true
                anchors.fill: parent
                background: Rectangle {
                    color: "#eaeaea"
                }
            }
        }
        
        Label {
            Layout.alignment: Qt.AlignCenter
            //Layout.fillWidth: true
            Layout.columnSpan: 2
            text: "<b>Погода сейчас<b>"
            color: "#ffab25"
        }
        RowLayout{
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Layout.alignment: Qt.AlignCenter
            
            TextField {
                id: textField
                
                objectName: "textField"
                color: "#ffab25"
                horizontalAlignment: Text.AlignHCenter
                readOnly: true
                Layout.alignment: Qt.AlignCenter
                Layout.columnSpan: 2
            }
            
            
            //TextField {
            //    id: textFieldDate
            //    objectName: "textFieldDate"
            //   readOnly: true
            //   Layout.alignment: Qt.AlignCenter
            //   Layout.columnSpan: 2
            
            // }
            
            
        }
    }
}
