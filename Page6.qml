import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12

Page{
    id: page6
    header: ToolBar{
        contentHeight: 15
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 30
            implicitWidth: 100
            //главная картинка
            Image {
                id: image2222
                height: 102
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                fillMode: Image.Stretch
                source: "jell.png"
                //иконка галочек
                Image {
                    id: image1222
                    width: 50
                    height: 36
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 217
                    anchors.left: parent.left
                    anchors.leftMargin: 217
                    fillMode: Image.PreserveAspectFit
                    source: "tap.png"
                }
                //название лабы
                Label {
                    id: label222
                    height: 56
                    color: "#deffffff"
                    text: qsTr("Лабораторная работа №6.
    ")
                    font.pointSize: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    font.weight: Font.DemiBold
                    font.family: "Tahoma"
                    font.bold: true
                    styleColor: "#ffffff"
                    textFormat: Text.AutoText
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
    Rectangle {
        id: rectangle122
        color: "#1d2027"
        anchors.topMargin: 0
        anchors.fill: parent
    }

    ScrollView{
            anchors.topMargin: 77
            anchors.fill: parent
            Image{
                id: background

                width: parent.width
                height: parent.height*2
                sourceSize.width: -1
                fillMode: Image.TileHorizontally
                y: -grid.contentY / 3 | -list.contentY / 3
            }


        ColumnLayout{

            anchors.fill: parent
            Layout.alignment: Qt.AlignHCenter

        RowLayout{
            Layout.alignment: Qt.AlignHCenter | Qt.AlignTop

            RadioButton{ // выбор, что отображается на страничке
                id: rad_1 // id
                //Layout.alignment: Qt.AlignLeft
                text: "Столбцы" // подпись

                checked: true // выбран по умолчанию
                onCheckedChanged: // если сменен выбор
                    if(rad_1.checked === true){ // если выбран rad1
                        grid.visible = true
                        list.visible = false
                    }

            }
            RadioButton{ // выбор, что отображается на страничке
                id: rad_2 // id
                //Layout.alignment: Qt.AlignRight
                text: "Строки" // подпись
                checked: false
                onCheckedChanged: // если вменен выбор
                    if(rad_2.checked === true){ // если выбран rad2
                        list.visible = true // видео не отображается
                        grid.visible = false // слайдер не отображается
                    }
            }
        }


            GridView{
                id: grid
                visible: true
                Layout.fillHeight: true
                Layout.fillWidth: true
                enabled: true
                cellHeight: 150
                cellWidth: 300
                model: app_model

                //spacing: 10

                delegate: Rectangle{
                    color: "#1d2027"
                    width: 300
                    height: 150
                    border.color: "#e89d36"
                    Layout.margins: 10
                    GridLayout{
                        anchors.fill: parent
                        columns: 3
                        rows: 3
                        //Layout.margins: 20
                        Image{
                            source: pic50x50
                            Layout.column: 0
                            Layout.row: 0
                            Layout.rowSpan: 3
                            Layout.fillHeight: true
                            Layout.preferredWidth: 100
                            Layout.margins: 5
                            fillMode: Image.PreserveAspectFit
                        }
                        Label{ // короткое название
                            color: "#e89d36"
                            text: shortname
                            Layout.column: 1
                            Layout.row: 1
                            Layout.fillHeight: true
                            Layout.preferredWidth: 100
                            //Layout.margins: 20
                        }
                        Label{ // название
                            color: "#e89d36"
                            text: name
                            Layout.column: 1
                            Layout.row: 2
                            Layout.fillHeight: true
                            Layout.preferredWidth: 100
                            //Layout.margins: 20
                        }
                        Label{ // ID
                            color: "#e89d36"
                            text: "Кол-во коментов " + appid
                            Layout.column: 1
                            Layout.row: 3
                            Layout.fillHeight: true
                            Layout.preferredWidth: 100
                            //Layout.margins: 20
                        }
                    }
                }
            }

            ListView{
                id: list
                visible: false
                Layout.fillHeight: true
                Layout.fillWidth: true
                enabled: true
                model: app_model
                spacing: 30

            delegate: Rectangle{
                            color: "#1d2027"
                            width: 600
                            height: 100
                            border.color: "#e89d36"
                            Layout.margins: 10
                            GridLayout{
                                anchors.fill: parent
                                columns: 3
                                rows: 3
                                //Layout.margins: 20
                                Image{
                                    source: pic50x50
                                    Layout.column: 0
                                    Layout.row: 0
                                    Layout.rowSpan: 3
                                    Layout.fillHeight: true
                                    Layout.preferredWidth: 100
                                    Layout.margins: 5
                                    fillMode: Image.PreserveAspectFit
                                }
                                Label{ // короткое название
                                    color: "#e89d36"
                                    text: shortname
                                    Layout.column: 1
                                    Layout.row: 1
                                    Layout.fillHeight: true
                                    Layout.preferredWidth: 100
                                    //Layout.margins: 20
                                }
                                Label{ // название
                                    color: "#e89d36"
                                    text: name
                                    Layout.column: 2
                                    Layout.row: 1
                                    Layout.fillHeight: true
                                    Layout.preferredWidth: 100
                                    //Layout.margins: 20
                                }
                                Label{ // ID
                                    color: "#e89d36"
                                    text: "Кол-во коментов " + appid
                                    Layout.column: 3
                                    Layout.row: 1
                                    Layout.fillHeight: true
                                    Layout.preferredWidth: 100
                                    //Layout.margins: 20
                                }
                            }
                        }
                    }
                }
                }
}
