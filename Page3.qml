import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2
import QtMultimedia 5.4
import QtWinExtras 1.0
import QtQuick.Dialogs 1.0
import QtGraphicalEffects 1.12

Page {
    visible: true
    id: page3
    header: ToolBar{
        contentHeight: 15
        anchors.top: parent.top
        background: Rectangle{
            implicitHeight: 30
            implicitWidth: 100
            //главная картинка
            Image {
                id: image22
                height: 102
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.top: parent.top
                anchors.topMargin: 0
                fillMode: Image.Stretch
                source: "jell.png"
                //иконка галочек
                Image {
                    id: image12
                    width: 50
                    height: 36
                    anchors.top: parent.top
                    anchors.topMargin: 8
                    anchors.right: parent.right
                    anchors.rightMargin: 217
                    anchors.left: parent.left
                    anchors.leftMargin: 217
                    fillMode: Image.PreserveAspectFit
                    source: "tap.png"
                }
                //название лабы
                Label {
                    id: label22
                    height: 56
                    color: "#deffffff"
                    text: qsTr("Лабораторная работа №3. Графические эффекты
")
                    font.pointSize: 12
                    anchors.right: parent.right
                    anchors.rightMargin: 0
                    anchors.left: parent.left
                    anchors.leftMargin: 0
                    anchors.top: parent.top
                    anchors.topMargin: 50
                    font.weight: Font.DemiBold
                    font.family: "Tahoma"
                    font.bold: true
                    styleColor: "#ffffff"
                    textFormat: Text.AutoText
                    horizontalAlignment: Text.AlignHCenter
                }
            }
        }
    }
    Rectangle {
        id: rectangle12
        color: "#1d2027"
        anchors.topMargin: 0
        anchors.fill: parent
    }
    
    
    GridLayout{
        anchors.topMargin: 77
        anchors.fill: parent
        columns: 2
        rows: 6
        
        //                Rectangle{
        //                    Layout.row: 0
        //                    Layout.columnSpan: 2
        //                    color: "#ffab25"
        
        //                    height: 2
        //                    Layout.fillWidth: true
        //                    radius: 0
        
        
        //                }
        
        Item {
            Layout.row: 1
            Layout.column: 0
            Layout.alignment: Qt.AlignCenter
            
            width: 200
            height:150
            
            
            
            Image {
                id: bug
                source: "sobaka.jpg"
                width: 200
                height:150
                smooth: true
                visible: false
            }
            
            Image {
                id: mask
                source: "ThresholdMask_mask.png"
                width: 200
                height:150
                smooth: true
                visible: false
            }
            
            ThresholdMask {
                anchors.fill: bug
                source: bug
                maskSource: mask
                threshold: slider1.value
                spread: slider2.value
            }
            
            
            
            
        }
        
        ColumnLayout{
            Layout.row: 1
            Layout.column: 1
            
            Rectangle {
                id: rectangle33
                width: 100
                height: 20
                color: "#ffab25"
                radius: 10
                Layout.fillWidth: false
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                
                
                Label {
                    id: label13
                    width: 74
                    height: 13
                    color: "#000000"
                    text: qsTr("ThresholdMask")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
            
            
            Slider {
                id: slider1
                Text {
                    
                    color: "#ffab25"
                    
                    text: qsTr("threshold")
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                from: 0.0
                to: 1.0
                value: 0.5
                
            }
            Slider{
                id: slider2
                Text {
                    
                    color: "#ffab25"
                    
                    text: qsTr("spread")
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                from: 0.0
                to: 1.0
                stepSize: 0.2
                
            }
        }
        
        Rectangle{
            Layout.row: 2
            Layout.columnSpan: 2
            color: "#ffab25"
            
            height: 2
            Layout.fillWidth: true
            radius: 0
            
        }
        
        
        Item {
            width: 200
            height: 150
            Layout.row: 3
            Layout.column: 0
            Layout.alignment: Qt.AlignCenter
            
            
            Rectangle {
                anchors.fill: parent
            }
            
            Image {
                id: bug2
                source: "sobaka.jpg"
                width: 200
                height: 150
                smooth: true
                visible: false
                
            }
            
            BrightnessContrast {
                anchors.fill: bug2
                source: bug2
                brightness: slider4.value
                contrast: slider5.value
            }
        }
        
        ColumnLayout{
            Layout.row: 3
            Layout.column: 1
            
            Rectangle {
                id: rectangle34
                width: 100
                height: 20
                color: "#ffab25"
                radius: 10
                Layout.fillWidth: false
                Label {
                    id: label34
                    width: 84
                    height: 13
                    color: "#000000"
                    text: qsTr("BrightnessContr")
                    horizontalAlignment: Text.AlignHCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            
            Slider {
                id: slider4
                Text {
                    
                    color: "#ffab25"
                    
                    text: qsTr("brightness")
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                from: 0.0
                to: 1.0
                stepSize: 0.1
                
            }
            Slider {
                id: slider5
                Text {
                    
                    color: "#ffab25"
                    
                    text: qsTr("contrast")
                    anchors.horizontalCenter: parent.horizontalCenter
                }
                from: 0.0
                to: 1.0
                stepSize: 0.1
                
            }
            
        }
        
        Rectangle{
            Layout.row: 4
            Layout.columnSpan: 2
            color: "#ffab25"
            
            height: 2
            Layout.fillWidth: true
            radius: 0
            
            
        }
        
        Item {
            width: 200
            height: 150
            Layout.row: 5
            Layout.column: 0
            Layout.alignment: Qt.AlignCenter
            
            
            
            Image {
                id: bug1
                source: "sobaka.jpg"
                width: 200
                height: 150
                smooth: true
                visible: false
                
            }
            
            Image {
                id: mask1
                source: "mask1.png"
                width: 200
                height: 150
                smooth: true
                visible: false
            }
            
            OpacityMask {
                anchors.fill: bug1
                source: bug1
                maskSource: mask1
                invert: invert.checked
                
            }
        }
        
        ColumnLayout{
            Layout.row: 5
            Layout.column: 1
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillHeight: false
            Layout.fillWidth: false
            
            
            Rectangle {
                id: rectangle35
                width: 100
                height: 20
                color: "#ffab25"
                radius: 10
                Layout.fillWidth: false
                Label {
                    id: label35
                    width: 62
                    height: 13
                    color: "#000000"
                    text: qsTr("OpacityMask")
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
            
            Switch{
                id:invert
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                
            }
            
            
        }
    }
}
